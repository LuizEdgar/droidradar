package com.luizedgar.droidradar.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.luizedgar.droidradar.R;
import com.luizedgar.droidradar.model.Layer;

public class LayersAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<Layer> layers;
	private LayerViewHolder holder;
	
	public LayersAdapter(Context context, ArrayList<Layer> layers){
		this.layers = layers;
		mInflater = LayoutInflater.from(context);	
	}
	
	@Override
	public int getCount() {
		return layers.size();
	}

	@Override
	public Layer getItem(int position) {
		return layers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Layer layer = layers.get(position);
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.single_layer, null);
			holder = new LayerViewHolder();
			holder.nomeLayer = (CheckedTextView) convertView.findViewById(R.id.layer_name);
		}
		
		//holder.nomeDuty.setText(Integer.valueOf(duty.getId()).toString()); //PELO ID
		holder.nomeLayer.setText(layer.getName()); //PELO NOME
		holder.nomeLayer.setChecked(layer.isActive());
		
		
		return convertView;
	}
	
	static class LayerViewHolder {
		public CheckedTextView nomeLayer;
	}
}
