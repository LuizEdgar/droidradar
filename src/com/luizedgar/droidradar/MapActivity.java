package com.luizedgar.droidradar;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luizedgar.droidradar.dao.LayerDAO;
import com.luizedgar.droidradar.dao.MarkerDAO;
import com.luizedgar.droidradar.model.Layer;
import com.luizedgar.droidradar.model.PlaceMarker;

public class MapActivity extends FragmentActivity implements LocationListener,
		OnSeekBarChangeListener {
	private static final double DEFAULT_RADIUS = 2000;
	private SeekBar bar;
	private ArrayList<Layer> layers = new ArrayList<Layer>();
	private LocationManager locationManager;
	private ArrayList<ArrayList<PlaceMarker>> marks = new ArrayList<ArrayList<PlaceMarker>>();
	private GoogleMap mMap;
	private UiSettings mUiSettings;
	private String provider;
	private Location myLocation;
	private Circle circle;
	private double myRange = DEFAULT_RADIUS;


	private void drawMarkers() {
		mMap.clear();
		PlaceMarker m;
		LatLng lg;
		Location markerLocation = new Location("Marker");
		for (int i = 0; i < marks.size(); i++) {
			if (layers.get(i).isActive()) {
				for (int j = 0; j < marks.get(i).size(); j++) {
					m = marks.get(i).get(j);
					markerLocation.setLatitude(m.getLatitude());
					markerLocation.setLongitude(m.getLongitude());
					if (markerLocation.distanceTo(myLocation) <= myRange) {
						lg = new LatLng(m.getLatitude(), m.getLongitude());
						mMap.addMarker(new MarkerOptions().position(lg)
								.title(m.getDescription())
								.icon(getMarkerIcon(m.getType())));
					}
				}
			}
		}
	}

	private BitmapDescriptor getMarkerIcon(String type) {
		if (type.equals("fixo100")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo100);
		} else if (type.equals("fixo110")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo110);
		} else if (type.equals("fixo120")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo120);
		} else if (type.equals("fixo30")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo30);
		} else if (type.equals("fixo40")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo40);
		} else if (type.equals("fixo50")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo50);
		} else if (type.equals("fixo70")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo70);
		} else if (type.equals("fixo60")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo60);
		} else if (type.equals("fixo90")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.fixo90);
		} else if (type.equals("lombada")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.lombada);
		} else if (type.equals("movel100")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel100);
		} else if (type.equals("movel110")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel110);
		} else if (type.equals("movel120")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel120);
		} else if (type.equals("movel30")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel30);
		} else if (type.equals("movel40")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel40);
		} else if (type.equals("movel50")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel50);
		} else if (type.equals("movel60")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel60);
		} else if (type.equals("movel70")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel70);
		} else if (type.equals("movel80")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel80);
		} else if (type.equals("movel90")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.movel90);
		} else if (type.equals("perigo")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.perigo);
		} else if (type.equals("pegadio")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.pedagio);
		} else if (type.equals("prf")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.prf);
		} else if (type.equals("semaforo30")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo30);
		} else if (type.equals("semaforo40")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo40);
		} else if (type.equals("semaforo50")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo50);
		} else if (type.equals("semaforo60")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo60);
		} else if (type.equals("semaforo70")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo70);
		} else if (type.equals("semaforo80")) {
			return BitmapDescriptorFactory.fromResource(R.drawable.semaforo80);
		} else {
			return BitmapDescriptorFactory.fromResource(R.drawable.perigo);
		}
	}

	private boolean isGooglePlay() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

		if (status == ConnectionResult.SUCCESS) {
			return (true);
		} else {
			Toast.makeText(this, "Google Play n�o est� dispon�vel",
					Toast.LENGTH_LONG).show();
		}
		return (false);
	}

	private void loadMarkers() {
		InputStream is = this.getResources().openRawResource(R.raw.droidradar);
		MarkerDAO mDao = new MarkerDAO(this, is);

		marks.add(mDao.selectPlaceMarks("layer_id = 1"));
		marks.add(mDao.selectPlaceMarks("layer_id = 2"));
		marks.add(mDao.selectPlaceMarks("layer_id = 3"));
		marks.add(mDao.selectPlaceMarks("layer_id = 4"));
		marks.add(mDao.selectPlaceMarks("layer_id = 5"));
		marks.add(mDao.selectPlaceMarks("layer_id = 6"));
		marks.add(mDao.selectPlaceMarks("layer_id = 7"));

		mDao.close();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isGooglePlay()) {
			setContentView(R.layout.activity_map);
			// Carrega o mapa caso precise.
			setUpMapIfNeeded();

			// Ativar bot�o de minha localiza��o
			mMap.setMyLocationEnabled(true);
			mUiSettings.setMyLocationButtonEnabled(true);
			// Desativar rota��o
			mUiSettings.setRotateGesturesEnabled(false);

			bar = (SeekBar) findViewById(R.id.seekBar1); // make seekbar object
			bar.setProgress(50);
			bar.setMax(100);
			bar.setOnSeekBarChangeListener(this);


			// Verificar se GPS est� ligado, caso n�o esteja, ele vai para as
			// configura��es deo sistema
			LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
			boolean enabledGPS = service
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			if (!enabledGPS) {
				Toast.makeText(this, "GPS offline, ative-o agora!",
						Toast.LENGTH_LONG).show();
				Intent intent = new Intent(
						Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, true);
			myLocation = locationManager.getLastKnownLocation(provider);

			loadMarkers();
			updateLayers();
			drawMarkers();
			updateCircle(myLocation);

			if (myLocation != null) {
				Log.i("DroidRadar", "Provider: " + provider);
				onLocationChanged(myLocation);
			} else {
				// --
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	// Move a c�mera para a localiza��o atual com zoom de 15.0f
	@Override
	public void onLocationChanged(Location location) {
		myLocation = location;
		double lat = location.getLatitude();
		double lng = location.getLongitude();
		Log.i("DroidRadar", "Nova Location " + lat + "," + lng);
		LatLng coordinate = new LatLng(lat, lng);
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f));
		drawMarkers();
		updateCircle(location);
	}

	public void updateCircle(Location location) {

		LatLng coordinate = new LatLng(location.getLatitude(),
				location.getLongitude());
		if (circle != null) {
			circle.remove();
		}
		circle = mMap.addCircle(new CircleOptions().center(coordinate)
				.strokeColor(Color.argb(70, 55, 108, 194))
				.fillColor(Color.argb(20, 55, 108, 194)).strokeWidth(5.0f)
				.radius(myRange));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case (R.id.layers_list_settings):
			// Abre as configura��es de Layers
			Intent i = new Intent(this, LayersActivity.class);
			startActivity(i);
	
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void defaultZoom(View view){
		mMap.animateCamera(CameraUpdateFactory.zoomTo(12.5f));
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	@Override
	public void onProviderDisabled(String provider) {
		String oldProvider = provider;
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, true);
		Toast.makeText(this,
				oldProvider + "foi desativado, o novo provider �:" + provider,
				Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onProviderEnabled(String provider) {
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, true);
		Toast.makeText(this, "Provider alterado para um melhor:" + provider,
				Toast.LENGTH_SHORT).show();

	}

	@Override
	protected void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(provider, 1000, 10, this);
		updateLayers();
		drawMarkers();
		Log.i("DroidRadar", layers.get(0).isActive() ? "ativo" : "nao");

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	private void setUpMap() {
		mMap.setMyLocationEnabled(true);
		mUiSettings = mMap.getUiSettings();
	}

	private void setUpMapIfNeeded() {
		if (mMap == null) {

			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			if (mMap != null) {
				setUpMap();
			}
		}
	}

	private void updateLayers() {
		LayerDAO dao = new LayerDAO(this);
		layers = dao.selectLayers();
		dao.close();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		myRange = DEFAULT_RADIUS * progress / 100;
		Log.i("DroidRadar", progress + "");
		updateCircle(myLocation);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		drawMarkers();
		updateCircle(myLocation);
	}

	

}
