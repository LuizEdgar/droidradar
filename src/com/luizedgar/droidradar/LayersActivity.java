package com.luizedgar.droidradar;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.luizedgar.droidradar.adapter.LayersAdapter;
import com.luizedgar.droidradar.dao.LayerDAO;
import com.luizedgar.droidradar.model.Layer;

public class LayersActivity extends Activity {

	private ListView layersList;
	private LayersAdapter adapter;
	private ArrayList<Layer> layers = new ArrayList<Layer>();
	
	private ListView getLayersList(){
		if(layersList == null){
			layersList = (ListView) findViewById(R.id.layers_list);
		}
		return layersList;
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layers);
		
		LayerDAO dao = new LayerDAO(this);
		layers = dao.selectLayers();
		dao.close();
		adapter = new LayersAdapter(this, layers);
		getLayersList().setAdapter(adapter);
		
		getLayersList().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position,
					long id) {
				CheckedTextView cv = (CheckedTextView) v;
				cv.toggle();
				Layer l = (Layer) adapter.getItemAtPosition(position);
				l.setActive(cv.isChecked());
				LayerDAO dao = new LayerDAO(getApplicationContext());
				dao.updateDone(l);
				dao.close();
				
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.layers, menu);
		return true;
	}

}
