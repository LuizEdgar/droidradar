package com.luizedgar.droidradar.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.luizedgar.droidradar.model.PlaceMarker;

public class MarkerDAO extends SQLiteOpenHelper {
	
	private static int VERSION = 1;
	private static String TABELA = "placemarks";
	private static String[] COLS = {"id","description","type", "longitude", "latitude", "heading", "range",
									"layer_id"};
	InputStream fl;
	
	public MarkerDAO(Context context, InputStream inputStream) {
		super(context, TABELA, null, VERSION);
		// TODO Auto-generated constructor stub
		this.fl = inputStream;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
//		"heading" INTEGER,"range" INTEGER,"layer_id" INTEGER NOT NULL, FOREIGN KEY(layer_id) REFERENCES layers(_id));

		String sql = "CREATE TABLE " + TABELA +
				"( id INTEGER PRIMARY KEY," +
				"description TEXT NOT NULL," +
				"type TEXT NOT NULL," +
				"longitude REAL NOT NULL," +
				"latitude REAL NOT NULL," +
				"heading INTEGER," +
				"range INTEGER," +
				"layer_id INTEGER NOT NULL," +
				"FOREIGN KEY(layer_id) REFERENCES layer(id)" +
				");";
		db.execSQL(sql);
		
		try {
			
			populateMarksCE(db);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void populateMarksCE( SQLiteDatabase db) throws SQLException, IOException{
		String sql;
		String cl;
		BufferedReader bf;
		Log.i("BD", "Aqui");
		
		try {
			bf = new BufferedReader(new InputStreamReader(fl));

			while((cl = bf.readLine()) != null){
				Log.i("BD",cl);

				sql = cl;
				db.execSQL(sql);
			}		
		} catch (FileNotFoundException e) {
			Log.i("BD","Arquivo n�o encontrado!");
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ MarkerDAO.TABELA);
		this.onCreate(db);				
	}
	
	public ArrayList<PlaceMarker> selectPlaceMarks(String where){
		//"id","description","type", "longitude", "latitude", "heading", "range","layer_id"
		ArrayList<PlaceMarker> marks = new ArrayList<PlaceMarker>();
		Cursor c = getWritableDatabase().query(TABELA, COLS, where, null, null, null, "id DESC");

		while(c.moveToNext()){
			PlaceMarker l = new PlaceMarker();
			l.setId(c.getInt(0));
			l.setDescription(c.getString(1));
			l.setType(c.getString(2));
			l.setLongitude(c.getDouble(3));
			l.setLatitude(c.getDouble(4));
			l.setHeading(c.getInt(5));
			l.setRange(c.getInt(6));
			l.setLayer_id(c.getInt(7));
			marks.add(l);
		}
		c.close();
		
		return marks;
	}

}
