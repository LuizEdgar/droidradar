package com.luizedgar.droidradar.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.luizedgar.droidradar.model.Layer;

public class LayerDAO extends SQLiteOpenHelper {
	
	private static int VERSION = 1;
	private static String TABELA = "layer";
	private static String[] COLS = {"id","name","active"};

	public LayerDAO(Context context) {
		super(context, TABELA, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE " + TABELA +
					"( id INTEGER PRIMARY KEY," +
					"name TEXT NOT NULL," +
					"active INTEGER NOT NULL" +
					");";
		db.execSQL(sql);
		populateLayersCE(db);
	}
	
	private void populateLayersCE( SQLiteDatabase db){
		String sql = "INSERT INTO 'layer' (name, active) VALUES ('Radar Fixo', 1)," +
				"('Radar Movel', 1)," +
				"('Semaforo com Camera', 1)," +
				"('Semaforo com Radar', 1)," +
				"('Policia Rodoviaria', 1)," +
				"('Pedagio', 1)," +
				"('Lombada', 1);";
		db.execSQL(sql);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ LayerDAO.TABELA);
		this.onCreate(db);			
	}
	
	public void updateDone(Layer layer){
		ContentValues values = new ContentValues();
		values.put("active",layer.isActive()?1:0);
		getWritableDatabase().update(TABELA, values, "id = '"+layer.getId()+"'", null);
	}
	
	public ArrayList<Layer> selectLayers(){
		ArrayList<Layer> layers = new ArrayList<Layer>();
		Cursor c = getWritableDatabase().query(TABELA, COLS, null, null, null, null, null);

		while(c.moveToNext()){
			Layer l = new Layer();
			l.setId(c.getInt(0));
			l.setName(c.getString(1));
			l.setActive(c.getInt(2) != 0);
			layers.add(l);
		}
		c.close();
		
		return layers;
	}

}
